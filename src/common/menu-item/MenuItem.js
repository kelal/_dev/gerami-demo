import React from 'react'

import './MenuItem.scss'
import Anchor from '../anchor/Anchor'

export default ({ children, className, ...rest }) => (
  <Anchor
    button
    {...rest}
    className={`menu-item${className ? ' ' + className : ''}`}
  >
    {children}
  </Anchor>
)
