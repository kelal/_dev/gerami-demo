import React, { StrictMode } from 'react'
import ReactDOM from 'react-dom'
import initReactFastclick from 'react-fastclick'

import './styles/index.scss'
import App from './app/App'
import registerServiceWorker from './registerServiceWorker'

initReactFastclick()

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root')
)

registerServiceWorker()
